const getSum = (str1, str2) => {

  if (typeof str1 !== 'string' || typeof str2 !== 'string' || isNaN(+str1) || isNaN(+str2)) {
    return false;
  }

const str1Length = str1.length
const str2Length = str2.length
const MaxLength = Math.max(str1Length, str2Length)

let carry = 0, sum = ''

for (let i = 1; i <= MaxLength; i++) {
  let a = +str1.charAt(str1Length - i)
  let b = +str2.charAt(str2Length - i)

  let t = carry + a + b
  carry = t/10 |0
  t %= 10

  sum = (i === MaxLength && carry)
    ? carry*10 + t + sum
    : t + sum
}

return sum ? sum : false;
};


const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  const postByAuthor = listOfPosts.filter(post => post.author === authorName);
  const commentsByAuthor = listOfPosts.reduce((acc, post) => {
    const numberOfPosts = post.comments ? post.comments.filter(comment => comment.author === authorName).length : 0;
    acc += numberOfPosts;
    return acc;
  }, 0)
  return `Post:${postByAuthor.length},comments:${commentsByAuthor}`
};


const tickets=(people)=> {

  let canSell = 'YES'

  people.reduce((acc, amount) => {
    if (+amount === 25) {
      acc.push(25);
      return acc;
    }
    if (+amount === 50 && acc.includes(25)) {
      acc.push(50);
      acc.splice(acc.indexOf(25), 1);
      return acc;
    } 
    if (+amount === 100 && acc.filter(num => num === 25).length >= 3 || acc.includes(50) && acc.includes(25)) {
      acc.push(100);
      if (acc.includes(50) && acc.includes(25)) {
        acc.splice(acc.indexOf(50), 1);
        acc.splice(acc.indexOf(25), 1);
      }
      for (let i = 0; i < 3; i++) {
        acc.splice(acc.indexOf(25), 1);
      }
      return acc;
    } 
      canSell = "NO";
      return acc;
  }, [])
  return canSell;
}

module.exports = {getSum, getQuantityPostsByAuthor, tickets};
